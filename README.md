# MongoDB
mongo database for development environment.

### Requirements
* [Docker](http://docker.io/)
* [Docker compose](https://github.com/docker/compose/releases)
* [mongo](https://hub.docker.com/r/library/mongo/)

### Instructions
#### Startup services
```
  docker-compose up db
```

##### Enter to mongo shell
```
    docker-compose exec db mongo [db-name]
```

#### TODOs
* create user


### WebUI
* [x] [mongo-express](https://hub.docker.com/r/library/mongo-express/)
* [] [adminmongo](https://hub.docker.com/r/mrvautin/adminmongo/)
* [] [mongoclient](https://hub.docker.com/r/mongoclient/mongoclient/)
